# Probability of having k floods in N years

Demo ; simple interactive tool to show the probability of occurence of k floods with a return period superior to T in N years.
It also displays the addition from 0 to k, i.e.  the probability of k such floods at most.